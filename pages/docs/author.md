---
title: About me and this website
excerpt: "Profile page of painter Hephaestionas Stavrou (Ηφαιστίωνας Σταύρου)"
permalink: /author/
layout: page
---

+ Name: Hephaestionas (Ηφαιστίωνας)
+ Birthyear: 1995
+ Birthplace: Greece
+ Email: <art@hephaestionas.com>

I am a professional painter.  This website holds samples of my work.  If
you want to purchase one of my paintings, you are welcome to send me an
email.
