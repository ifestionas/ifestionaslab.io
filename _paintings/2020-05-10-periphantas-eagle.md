---
title: "Periphantas the eagle"
excerpt: "Oil on canvas of Periphantas."
image: "20200510_periphantas_eagle.jpg"
painting:
    material: "Oil on canvas"
    dimensions:
        height: 100
        width: 70
    produced: 2020-05-10
    itemno: "20200510pe"
---
