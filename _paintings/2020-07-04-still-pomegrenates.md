---
title: "Pomegrenates"
excerpt: "Pomegrenates still nature: oil on canvas."
image: "20200704_still_pomegrenates.jpg"
painting:
    material: "Oil on canvas"
    dimensions:
        height: 24
        width: 30
    produced: 2020-07-04
    itemno: "20200704po"
---
