---
title: "Peaches"
excerpt: "Peaches still nature: oil on canvas."
image: "20200629_still_peaches.jpg"
painting:
    material: "Oil on canvas"
    dimensions:
        height: 24
        width: 30
    produced: 2020-06-29
    itemno: "20200629pe"
---
