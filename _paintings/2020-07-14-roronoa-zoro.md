---
title: "Roronoa Zoro"
excerpt: "Oil on canvas of a Samurai."
image: "20200714_roronoa_zoro.jpg"
painting:
    material: "Oil on canvas"
    dimensions:
        height: 50
        width: 40
    produced: 2020-07-14
    itemno: "20200714rz"
---
