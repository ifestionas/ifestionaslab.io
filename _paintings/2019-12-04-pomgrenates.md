---
title: "Pomgrenates"
date: 2019-12-04
excerpt: "Pomegrenates still nature: oil on canvas."
image: "20191204_pomegranates.jpg"
painting:
    material: "Oil on canvas"
    dimensions:
        height: 20
        width: 60
    produced: 2019-12-04
    itemno: "20191204p"
---
