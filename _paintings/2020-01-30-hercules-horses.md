---
title: "Hercules"
excerpt: "Sketch with pencil."
image: "20200130_hercules_horses_copy_sketch.jpg"
painting:
    produced: 2020-02-17
    notice: "Copy of Henri Regnault"
---
