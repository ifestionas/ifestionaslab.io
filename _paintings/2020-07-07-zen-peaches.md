---
title: "Zen peaches"
excerpt: "Oil on canvas: still nature with peaches."
image: "20200707_zen_peaches.jpg"
painting:
    material: "Oil on canvas"
    dimensions:
        height: 20
        width: 60
    produced: 2020-07-07
    itemno: "20200707zp"
---
