---
title: "Worker's boots"
excerpt: "Oil on canvas, depicting a pair of boots."
image: "20200707_worker_boots.jpg"
painting:
    material: "Oil on canvas"
    dimensions:
        height: 40
        width: 40
    produced: 2020-07-06
    itemno: "20200706wb"
---
