---
title: "Mount Fuji"
excerpt: "Oil on board of Mount Fuji."
image: "20201002_mount_fuji.jpg"
painting:
    material: "Oil on board"
    dimensions:
        height: 26
        width: 45
    produced: 2020-10-02
    itemno: "20201002mj"
---
