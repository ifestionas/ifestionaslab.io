---
title: "Need for expression"
date: 2019-12-27
excerpt: "Abstract oil on canvas."
image: "20191227_behind_the_mask.jpg"
painting:
    material: "Oil on canvas"
    dimensions:
        height: 100
        width: 120
    produced: 2019-12-27
    itemno: "20191227bm"
---
