---
title: "Goddess Aphrodite's birthplace"
excerpt: "Oil on board of Goddess Aphrodite's birthplace."
image: "20201008_aphrodite_birthplace.jpg"
painting:
    material: "Oil on board"
    dimensions:
        height: 46
        width: 53
    produced: 2020-10-08
    itemno: "20201008ab"
---
