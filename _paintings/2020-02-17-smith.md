---
title: "Smith"
excerpt: "Sketch with pencil."
image: "20200217_smith_copy_sketch.jpg"
painting:
    produced: 2020-02-17
    notice: "Copy of Pompeo Batoni"
---
