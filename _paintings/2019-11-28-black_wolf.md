---
title: "Lone black wolf"
date: 2019-11-28
excerpt: "Black wolf: oil on canvas."
image: "20191128_black_wolf.jpg"
painting:
    material: "Oil on canvas"
    dimensions:
        height: 60
        width: 40
    produced: 2019-11-28
    itemno: "20191128bw"
---
